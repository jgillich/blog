---
title: Myprotein Review
date: 2018-07-15
---

Myprotein is a fairly large European sports nutrition brand. Some time ago, they've also entered
the powdered meal replacements market with Whole Fuel, which is "inspired" by Huel.

They also offer meal replacement bars, which I will be reviewing as well.

## Whole Fuel

Whole Fuel's nutrition profile is a bit odd, 3 servings (100g each) provide you with
100% of your daily nutrients, but only 1300 calories. This is so that you can consume
additional calorie-rich supplements such as protein shakes. I usually consume about 2 shakes per day,
plus some other foods and/or protein shakes. But I've also had days where I only consumed
3 shakes of Whole Fuel and didn't feel hungry, so it could be a good option for weight loss.

When mixing, Whole Fuel is extremely thick. You are supposed to mix 3 scoops with 350-500ml water.
I use 500ml water with only 2 scoops and it's already as thick as pudding. I would recommend
3 scoops with at least 700ml water, unfortunately my shaker bottle does not hold that much.

I use a Promixx 2.0 vortex mixer, which creates a smooth and enjoyable texture. Shaking by hand
results in lots of unpleasant chunks, it's advised to use a electrical mixer to prepare Whole Fuel.

![](/images/myprotein/wholefuel.jpg)

Whole Fuel comes in three flavours, all of them contain loads of sweeteners.

* Vanilla Raspberry: Too sweet. You can taste the raspberry but not the vanilla. This one is my favorite.
* Vanilla: Too sweet. Doesn't taste like vanilla at all, I don't like it.
* Chocolate: Less sweet, still too much though. Doesn't taste like chocolate, but it's alright.

The best thing about Whole Fuel is its price, even though Myprotein has a opaque pricing scheme where
they set higher prices to then run discounts at all times. You can pick up a 5kg bag for as low as 40 Euros,
which will provide 10 full days of nutrition at 2000 calories per day, meaning it costs 4 Euros per day.
That's even cheaper than the already quite affordable Jimmy Joy.

Overall, there are lots of good things about Whole Fuel. High quality ingredients, great texture,
great price. But they need to work on their flavouring and reduce the sweetness.

## Meal Replacement Bars

Myprotein's meal replacement bars are high on nutrients (at least 15% but not well balanced) but quite low on
calories (227 per bar). They also contain 20g of protein and 7.6g of sugar. I don't think they have
enough calories to deserve the name meal replacement bar, but they still keep me satiated for 1-2 hours.
Again, a good option for weight loss or people with an active lifestyle.

These bars are really handy, I usually don't eat breakfast, but now I always grab a bar when I
leave the house in the morning and consume it on the go.

![](/images/myprotein/bars.jpg)

There are again three flavours here:

* Salted Caramel: Delicious.
* Chocolate Fudge: Good, but a bit dry.
* Mocha: If you like coffee, you might like these. I do not.

You'll always want to drink a bit of water along with these bars.

Compared to similar bars, they're also fairly affordable. I've gotten them for as low as 14 Euros
per pack of 12, or about 9 Euros per day if you eat nothing but these bars (not recommended).

## Other Products

They occasionally give you free products with your order, so I thought I'd quickly mention these:

* Impact Whey Protein Vanilla: You can actually taste the vanilla and it's not too sweet either - this is
how Whole Fuel should taste.
* Baked Cookie: You can eat raw sugar and it will be less sweet than this cookie. I don't know what they were thinking.

Most of their snacks are ridiculously expensive and of questionable nutritional value.
Like a small bar of chocolate with added protein for 4 Euros.

## Conclusion

Overall I'm quite happy with Myprotein's products. They are tailored towards people who exercise
a lot and take protein supplements, but can also be used to loose weight. My only complaints are
the bad flavouring of Whole Fuel and the opaque pricing scheme of Myprotein in general.